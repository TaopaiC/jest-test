const nock = require('nock');
const superagent = require('superagent');
const agent = require('superagent-promise')(superagent, Promise);

describe('jest actions', function () {

  beforeEach(() => {
    nock.cleanAll();
    nock.enableNetConnect();

    console.log('agent');
    console.log(agent);
  });

  it('test /todos api', () => {
    const scope = nock('http://localhost:3000')
      .get('/todos')
      .reply(200, {
        body: { todos: ['do something'] },
      });

    return agent
      .get('http://localhost:3000/todos')
      .end()
      .then((response) => {
        console.log('response:', response.status);
        console.log('response:', response.body);
        
        // no result ?????????????
        expect(response.status).toBe(200);

      }, (error) => {
        fail('fail');
        console.log('error1:', error.status);
        throw error;
      }).then(
        () => {
          expect(scope.isDone()).toBeTruthy();
        }, (error) => {
          console.log('error:', error);
          fail('fail');
          throw error;
        }
      );
  });
});
